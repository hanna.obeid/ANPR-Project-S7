# Automatic Number Plate Recognition (ANPR) using YOLOv7 and pytesseract

## Overview

This project implements an Automatic Number Plate Recognition (ANPR) system using YOLOv7 and pytesseract on a Jupyter Notebook. YOLOv7 is used for object detection and pytesseract is used for text recognition. The system is trained on a dataset of number plates and can detect and recognize the number plate.

## Repository structure

1.  `Helpers` directory: containing an anotation conversion script and data augmentation notebook
2.  `Main` directory: containing the main ANPR notebook, the images, as well as the YOLOv7 necessary files

## Main Dependencies

The following libraries and packages are the main ones used in this project:

-   OpenCV
-   pytesseract
-   torch
-   numpy

## Usage

To run the ANPR system, follow these steps:

1. Clone this repository and open the `ANPR_TrainYOLOv7.ipynb` file in Jupyter Notebook, or use `ANPR_TrainYOLOv7-colab.ipynb` on Google Colab
2. Make sure all dependencies are installed.
3. Run all cells in the notebook to load the trained model and start the ANPR system.
4. The ANPR system can be tested by providing an image from the `/Main/taken_test_car_images` directory

## Results

The ANPR system is able to detect and recognize number plates with a high accuracy.

## Helpers

1. `convert_voc_to_yolo.py`:
    > This is a Python script that converts annotated images in PASCAL VOC format into the format required by YOLO object detection. It iterates over a list of directories containing annotated images, converts the annotations for each image, and saves the converted annotations in a specified output directory.
2. `images_aug.ipynb`:
    > This notebook is used to read images and its corresponding label files and performs data augmentation (such as Scale, Translate, Rotate, Shear) using the data_aug library.

## Useful Documents
- [Report](https://gitlab-student.centralesupelec.fr/hanna.obeid/ANPR-Project-S7/-/blob/main/Rapport_Projet.pdf)
- [Presentation](https://gitlab-student.centralesupelec.fr/hanna.obeid/ANPR-Project-S7/-/blob/main/Presentation_Projet.pdf)

## Contact
If you have any questions or feedback, please contact:

-   [hanna.obeid@student-cs.fr](hanna.obeid@student-cs.fr)
-   [itimed.ben-abbes@student-cs.fr](itimed.ben-abbes@student-cs.fr)
-   [nour.el-hassan@student-cs.fr](nour.el-hassan@student-cs.fr)
-   [samer.lahoud@student-cs.fr](samer.lahoud@student-cs.fr)
-   [tannous.khouri@student-cs.fr](tannous.khouri@student-cs.fr)
